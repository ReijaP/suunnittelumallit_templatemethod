import java.util.Random;
import java.util.Scanner;

public class GuessNumber extends Game{
	Scanner in = new Scanner(System.in);
	String[] animals = {"koira", "kissa", "hevonen", "lehmä", "aasi", "elefantti"};
	
	Random random;
	int index;
	public boolean end;
	public int winner;

	@Override
	void initializeGame() {
		System.out.println("Peli alkaa!\n\n");
		
		random = new Random();
		index = random.nextInt(animals.length);
		end = false;
	}

	@Override
	void makePlay(int player) {
		System.out.println("\t Vuorossa pelaaja " + player);
		System.out.println("Mitä eläintä ajattelen:");
		for (int i = 0; i < animals.length; i++) {
			System.out.print(animals[i] + " ");
		}
		System.out.println("?");
		String choice = in.nextLine();
		
		if(choice.equals(animals[index])) {
			System.out.println("Onneksi olkoon, vastasit oikein!");
			end = true;
			winner = player;
		}
		
	}

	@Override
	boolean endOfGame() {
		return end;
	}

	@Override
	void printWinner() {
		System.out.println("Voittaja on pelaaja numero " + winner);
		
	}

}
